package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = NumberUtil.generateId();

    private static final String USER_ID_2 = NumberUtil.generateId();

    private static final String PROJECT_ID_1 = NumberUtil.generateId();

    private static final String PROJECT_ID_2 = NumberUtil.generateId();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task " + i);
            task.setDescription("description " + i);
            if (i <= 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            task.setProjectId(PROJECT_ID_2);
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddTask() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task newTask = new Task();
        taskRepository.add(newTask);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = NumberUtil.generateId();
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        taskRepository.add(userId, name, description);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            taskList.add(new Task());
        }
        taskRepository.addAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAddAllNull() {
        taskRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testAddTaskToProjectPositive() {
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final Task expectedTask = taskList.get(0);
        expectedTask.setProjectId(projectId);
        taskRepository.addTaskToProject(expectedTask.getId(), projectId);
        @Nullable final Task actualTask = taskRepository.findById(expectedTask.getId());
        Assert.assertEquals(expectedTask, actualTask);
    }

    @Test
    public void testAddTaskToProjectNegative() {
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final String taskId = NumberUtil.generateId();
        taskRepository.addTaskToProject(taskId, projectId);
        @Nullable final Task actualTask = taskRepository.findById(taskId);
        Assert.assertNull(actualTask);
    }

    @Test
    public void testAddTaskToProjectForUserPositive() {
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final Task expectedTask = taskList.get(0);
        @NotNull final String userId = expectedTask.getUserId();
        expectedTask.setProjectId(projectId);
        taskRepository.addTaskToProject(expectedTask.getId(), projectId);
        @Nullable final Task actualTask = taskRepository.findByIdForUser(userId, expectedTask.getId());
        Assert.assertEquals(expectedTask, actualTask);
    }

    @Test
    public void testAddTaskToProjectForUserNegative() {
        @NotNull final String otherUserId = NumberUtil.generateId();
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final Task expectedTask = taskList.get(0);
        expectedTask.setProjectId(projectId);
        taskRepository.addTaskToProject(expectedTask.getId(), projectId);
        @Nullable final Task actualTask = taskRepository.findByIdForUser(otherUserId, expectedTask.getId());
        Assert.assertNull(actualTask);
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        taskRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        taskRepository.clearForUser(USER_ID_1);
        Assert.assertEquals(emptyList, taskRepository.findAllForUser(USER_ID_1));
        Assert.assertNotEquals(emptyList, taskRepository.findAllForUser(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        taskRepository.clearForUser("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll();
        Assert.assertEquals(taskList, actualTaskList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAllForUser(USER_ID_1);
        Assert.assertEquals(taskList.subList(0, 5), actualTaskList);
    }

    @Test
    public void testFindAllByProjectIdPositive() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        @NotNull final Task task3 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task2.setProjectId(PROJECT_ID_1);
        task3.setProjectId(PROJECT_ID_2);
        @NotNull final List<Task> expectedList = new ArrayList<>();
        expectedList.add(task1);
        expectedList.add(task2);
        taskRepository.add(task1);
        taskRepository.add(task2);
        taskRepository.add(task3);
        Assert.assertEquals(expectedList, taskRepository.findAllByProjectId(PROJECT_ID_1));
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        @NotNull final List<Task> expectedList = new ArrayList<>();
        Assert.assertEquals(expectedList, taskRepository.findAllByProjectId(PROJECT_ID_1));
    }

    @Test
    public void testFindAllByProjectIdForUser() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task1.setUserId(USER_ID_1);
        task2.setProjectId(PROJECT_ID_1);
        task2.setUserId(USER_ID_2);
        @NotNull final List<Task> expectedList = new ArrayList<>();
        expectedList.add(task1);
        taskRepository.add(task1);
        taskRepository.add(task2);
        Assert.assertEquals(expectedList, taskRepository.findAllByProjectIdForUser(USER_ID_1, PROJECT_ID_1));
    }

    @Test
    public void testFindAllByProjectIdForUserNegative() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task1.setUserId(USER_ID_1);
        task2.setProjectId(PROJECT_ID_1);
        task2.setUserId(USER_ID_1);
        @NotNull final List<Task> expectedList = new ArrayList<>();
        taskRepository.add(task1);
        taskRepository.add(task2);
        Assert.assertEquals(expectedList, taskRepository.findAllByProjectIdForUser(USER_ID_2, PROJECT_ID_1));
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.findById(task.getId()));
            Assert.assertEquals(task, taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(taskRepository.findById(id));
    }

    @Test
    public void testFindByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(taskRepository.findByIdForUser(USER_ID_1, task.getId()));
                Assert.assertEquals(task, taskRepository.findById(task.getId()));
            } else Assert.assertNull(taskRepository.findByIdForUser(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expected3 = taskList.get(taskRepository.getSize() / 2);
        Assert.assertEquals(expected1, taskRepository.findByIndex(0));
        Assert.assertEquals(expected2, taskRepository.findByIndex(taskRepository.getSize() - 1));
        Assert.assertEquals(expected3, taskRepository.findByIndex(taskRepository.getSize() / 2));
        Assert.assertNull(taskRepository.findByIndex(taskRepository.getSize()));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        Assert.assertEquals(expected1, taskRepository.findByIndexForUser(USER_ID_1, 0));
        Assert.assertEquals(expected2, taskRepository.findByIndexForUser(USER_ID_2, 0));
        Assert.assertNull(taskRepository.findByIndexForUser("unknown user", 0));
    }

    @Test
    public void testFindByName() {
        for (@NotNull final Task task : taskList) {
            Assert.assertEquals(task, taskRepository.findByName(task.getName()));
        }
        Assert.assertNull(taskRepository.findByName("???"));
    }

    @Test
    public void testFindByNameForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertEquals(task, taskRepository.findByNameForUser(USER_ID_1, task.getName()));
            else Assert.assertNull(taskRepository.findByNameForUser(USER_ID_1, task.getName()));
        }
    }

    @Test
    public void testFindKeysForUser() {
        @NotNull final List<String> expectedList = new ArrayList<>();
        @NotNull final List<String> emptyList = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) expectedList.add(task.getId());
        }
        Assert.assertEquals(expectedList, taskRepository.findKeysForUser(USER_ID_1));
        Assert.assertEquals(emptyList, taskRepository.findKeysForUser("???"));
    }

    @Test
    public void testGetId() {
        int count = 0;
        for (@NotNull final Task task : taskList) {
            Assert.assertEquals(task.getId(), taskRepository.getId(task.getName()));
            Assert.assertEquals(task.getId(), taskRepository.getId(count));
            count++;
        }
        Assert.assertNull(taskRepository.getId("Unknown name"));
        Assert.assertNull(taskRepository.getId(taskRepository.getSize()));
    }

    @Test
    public void testGetIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertEquals(task.getId(), taskRepository.getIdForUser(USER_ID_1, task.getName()));
            else Assert.assertNull(task.getId(), taskRepository.getIdForUser(USER_ID_1, task.getName()));
        }
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(5, taskRepository.getSizeForUser(USER_ID_1));
        Assert.assertEquals(0, taskRepository.getSizeForUser("Unknown user"));
    }

    @Test
    public void testIsEmpty() {
        Assert.assertFalse(taskRepository.isEmpty());
        @NotNull ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.isEmpty());
        taskRepository.clear();
        Assert.assertTrue(taskRepository.isEmpty());
    }

    @Test
    public void testIsEmptyForUser() {
        Assert.assertFalse(taskRepository.isEmptyForUser(USER_ID_1));
        Assert.assertTrue(taskRepository.isEmptyForUser("Unknown user"));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(taskRepository.isNotFoundById(validId));
        Assert.assertTrue(taskRepository.isNotFoundById(invalidId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskList.get(0).getId();
        Assert.assertFalse(taskRepository.isNotFoundByIdForUser(USER_ID_1, validId));
        Assert.assertTrue(taskRepository.isNotFoundByIdForUser(USER_ID_2, validId));
    }

    @Test
    public void testIsNotFoundTaskInProject() {
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertTrue(taskRepository.isNotFoundTaskInProject(taskId, PROJECT_ID_1));
        Assert.assertFalse(taskRepository.isNotFoundTaskInProject(taskId, PROJECT_ID_2));
    }

    @Test
    public void testRemoveAllByProjectId() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task2.setProjectId(PROJECT_ID_1);
        @NotNull final List<Task> expectedList = new ArrayList<>();
        expectedList.add(task1);
        expectedList.add(task2);
        taskRepository.add(task1);
        taskRepository.add(task2);
        taskRepository.removeAllByProjectId(PROJECT_ID_2);
        Assert.assertEquals(expectedList, taskRepository.findAll());
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Task task : taskList) {
            taskRepository.remove(task);
            Assert.assertNull(taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Task taskNotFromRepository = new Task();
        taskRepository.remove(taskNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.removeById(task.getId()));
            Assert.assertNull(taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        Assert.assertNull(taskRepository.removeById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(taskRepository.removeByIdForUser(USER_ID_1, task.getId()));
                Assert.assertNull(taskRepository.findByIdForUser(USER_ID_1, task.getId()));
            } else {
                Assert.assertNull(taskRepository.removeByIdForUser(USER_ID_1, task.getId()));
            }
        }
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expected3 = taskList.get(taskRepository.getSize() / 2);
        Assert.assertEquals(expected1, taskRepository.removeByIndex(0));
        Assert.assertEquals(expected2, taskRepository.removeByIndex(taskRepository.getSize() - 1));
        Assert.assertEquals(expected3, taskRepository.removeByIndex(taskRepository.getSize() / 2));
        Assert.assertNull(taskRepository.removeByIndex(taskRepository.getSize()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 3, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        Assert.assertEquals(expected1, taskRepository.removeByIndexForUser(USER_ID_1, 0));
        Assert.assertEquals(expected2, taskRepository.removeByIndexForUser(USER_ID_2, 0));
        Assert.assertNull(taskRepository.removeByIndexForUser("unknown user", 0));
    }

    @Test
    public void testRemoveByName() {
        for (@NotNull final Task task : taskList) {
            Assert.assertEquals(task, taskRepository.removeByName(task.getName()));
        }
        Assert.assertNull(taskRepository.removeByName("???"));
        Assert.assertEquals(0, taskRepository.getSize());
    }

    @Test
    public void testRemoveByNameForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertEquals(task, taskRepository.removeByNameForUser(USER_ID_1, task.getName()));
            else Assert.assertNull(taskRepository.removeByNameForUser(USER_ID_1, task.getName()));
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES - 5, taskRepository.getSize());
    }

    @Test
    public void testRemoveTaskFromProject() {
        for (@NotNull final Task task : taskList) {
            Assert.assertEquals(task, taskRepository.removeTaskFromProject(task.getId(), PROJECT_ID_2));
            Assert.assertNull(taskRepository.removeTaskFromProject(task.getId(), PROJECT_ID_1));
        }
        Assert.assertEquals(0, taskRepository.findAllByProjectId(PROJECT_ID_2).size());
    }

    @Test
    public void testRemoveTaskFromProjectForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertEquals(task, taskRepository.removeTaskFromProjectForUser(USER_ID_1, task.getId(), PROJECT_ID_2));
            else
                Assert.assertNull(taskRepository.removeTaskFromProjectForUser(USER_ID_1, task.getId(), PROJECT_ID_2));
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES - 5, taskRepository.findAllByProjectId(PROJECT_ID_2).size());
    }

    @Test
    public void testUpdatePositive() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        taskExpected.setName(newName);
        taskExpected.setDescription(newDescription);
        @Nullable final Task taskActual = taskRepository.update(taskId, newName, newDescription);
        Assert.assertEquals(taskExpected, taskActual);
    }

    @Test
    public void testUpdateNegative() {
        @NotNull final String taskId = NumberUtil.generateId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        @Nullable final Task taskActual = taskRepository.update(taskId, newName, newDescription);
        Assert.assertNull(taskActual);
    }

    @Test
    public void testUpdateForUserPositive() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        taskExpected.setName(newName);
        taskExpected.setDescription(newDescription);
        @Nullable final Task taskActual = taskRepository.updateForUser(USER_ID_1, taskId, newName, newDescription);
        Assert.assertEquals(taskExpected, taskActual);
    }

    @Test
    public void testUpdateForUserNegative() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        taskExpected.setName(newName);
        taskExpected.setDescription(newDescription);
        @Nullable final Task taskActual = taskRepository.updateForUser(USER_ID_2, taskId, newName, newDescription);
        Assert.assertNull(taskActual);
    }

    @Test
    public void testUpdateStatusById() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        taskExpected.setStatus(Status.COMPLETED);
        @Nullable final Task taskActual = taskRepository.updateStatusById(taskId, Status.COMPLETED);
        Assert.assertNotNull(taskActual);
        Assert.assertEquals(taskExpected, taskActual);
        Assert.assertNotNull(taskActual.getDateFinish());
    }

    @Test
    public void testUpdateStatusByIdForUser() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        taskExpected.setStatus(Status.IN_PROGRESS);
        @Nullable final Task taskActual = taskRepository.updateStatusByIdForUser(USER_ID_1, taskId, Status.IN_PROGRESS);
        Assert.assertNotNull(taskActual);
        Assert.assertEquals(taskExpected, taskActual);
        Assert.assertNotNull(taskActual.getDateStart());
        Assert.assertNull(taskRepository.updateStatusByIdForUser(USER_ID_2, taskId, Status.IN_PROGRESS));
    }

    @Test
    public void testUpdateStatusByIndex() {
        @NotNull final Task taskExpected = taskList.get(0);
        taskExpected.setStatus(Status.COMPLETED);
        @Nullable final Task taskActual = taskRepository.updateStatusByIndex(0, Status.COMPLETED);
        Assert.assertNotNull(taskActual);
        Assert.assertEquals(taskExpected, taskActual);
        Assert.assertNotNull(taskActual.getDateFinish());
    }

    @Test
    public void testUpdateStatusByIndexForUser() {
        @NotNull final Task taskExpected = taskList.get(0);
        taskExpected.setStatus(Status.IN_PROGRESS);
        @Nullable final Task taskActual = taskRepository.updateStatusByIndexForUser(USER_ID_1, 0, Status.IN_PROGRESS);
        Assert.assertNotNull(taskActual);
        Assert.assertEquals(taskExpected, taskActual);
        Assert.assertNotNull(taskActual.getDateStart());
        Assert.assertNull(taskRepository.updateStatusByIndexForUser("unknown user", 0, Status.IN_PROGRESS));
    }

    @Test
    public void testUpdateStatusByName() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskName = taskExpected.getName();
        taskExpected.setStatus(Status.COMPLETED);
        @Nullable final Task taskActual = taskRepository.updateStatusByName(taskName, Status.COMPLETED);
        Assert.assertNotNull(taskActual);
        Assert.assertEquals(taskExpected, taskActual);
        Assert.assertNotNull(taskActual.getDateFinish());
    }

    @Test
    public void testUpdateStatusByNameForUser() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskName = taskExpected.getName();
        taskExpected.setStatus(Status.IN_PROGRESS);
        @Nullable final Task taskActual = taskRepository.updateStatusByNameForUser(USER_ID_1, taskName, Status.IN_PROGRESS);
        Assert.assertNotNull(taskActual);
        Assert.assertEquals(taskExpected, taskActual);
        Assert.assertNotNull(taskActual.getDateStart());
        Assert.assertNull(taskRepository.updateStatusByIdForUser(USER_ID_2, taskName, Status.IN_PROGRESS));
    }

}

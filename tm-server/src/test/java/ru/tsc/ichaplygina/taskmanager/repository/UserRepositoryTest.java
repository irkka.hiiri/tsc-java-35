package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user@" + i);
            user.setRole(Role.USER);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        userRepository.add(new User());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            userList.add(new User());
        }
        userRepository.addAll(userList);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testAddAllNull() {
        userRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        userRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> actualUserList = userRepository.findAll();
        Assert.assertEquals(userList, actualUserList);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findById(user.getId()));
            Assert.assertEquals(user, userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(userRepository.findById(id));
    }

    @Test
    public void testFindByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userRepository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindByLoginNegative() {
        @NotNull final String login = "unknown_login";
        Assert.assertNull(userRepository.findByLogin(login));
    }

    @Test
    public void testFindByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByEmail(user.getEmail()));
            Assert.assertEquals(user, userRepository.findByEmail(user.getEmail()));
        }
    }

    @Test
    public void testFindByEmailNegative() {
        @NotNull final String email = "unknown_email";
        Assert.assertNull(userRepository.findByEmail(email));
    }

    @Test
    public void testFindIdByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findIdByLogin(user.getLogin()));
            Assert.assertEquals(user.getId(), userRepository.findIdByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindIdByLoginNegative() {
        @NotNull final String login = "unknown_login";
        Assert.assertNull(userRepository.findIdByLogin(login));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testIsEmpty() {
        Assert.assertFalse(userRepository.isEmpty());
        @NotNull IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.isEmpty());
        userRepository.clear();
        Assert.assertTrue(userRepository.isEmpty());
    }

    @Test
    public void testIsFoundByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertTrue(userRepository.isFoundByEmail(user.getEmail()));
        }
    }

    @Test
    public void testIsFoundByEmailNegative() {
        Assert.assertFalse(userRepository.isFoundByEmail("unknown_email"));
    }

    @Test
    public void testIsFoundByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertTrue(userRepository.isFoundByLogin(user.getLogin()));
        }
    }

    @Test
    public void testIsFoundByLoginNegative() {
        Assert.assertFalse(userRepository.isFoundByLogin("unknown_login"));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final User user : userList) {
            userRepository.remove(user);
            Assert.assertNull(userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final User userNotFromRepository = new User();
        userRepository.remove(userNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.removeById(user.getId()));
            Assert.assertNull(userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        Assert.assertNull(userRepository.removeById(idNotFromRepository));
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (@NotNull final User user : userList) {
            userRepository.removeByLogin(user.getLogin());
            Assert.assertNull(userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testRemoveByLoginNegative() {
        userRepository.removeByLogin("unknown_login");
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testUpdatePositive() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String userId = userExpected.getId();
        @NotNull final String newLogin = "new_login";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userExpected.setLogin(newLogin);
        userExpected.setPasswordHash(newPassword);
        userExpected.setEmail(newEmail);
        userExpected.setRole(newRole);
        userExpected.setFirstName(newFirstName);
        userExpected.setMiddleName(newMiddleName);
        userExpected.setLastName(newLastName);
        @Nullable final User userActual = userRepository.update(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(userExpected, userActual);
    }

    @Test
    public void testUpdateNegative() {
        @NotNull final String userId = NumberUtil.generateId();
        @NotNull final String newLogin = "new_login";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        @Nullable final User userActual = userRepository.update(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
        Assert.assertNull(userActual);
    }

}

package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = NumberUtil.generateId();

    private static final String USER_ID_2 = NumberUtil.generateId();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project " + i);
            project.setDescription("description " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddProject() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project newProject = new Project();
        projectRepository.add(newProject);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = NumberUtil.generateId();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        projectRepository.add(userId, name, description);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projectList.add(new Project());
        }
        projectRepository.addAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAddAllNull() {
        projectRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        projectRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.clearForUser(USER_ID_1);
        Assert.assertEquals(emptyList, projectRepository.findAllForUser(USER_ID_1));
        Assert.assertNotEquals(emptyList, projectRepository.findAllForUser(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        projectRepository.clearForUser("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        Assert.assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAllForUser(USER_ID_1);
        Assert.assertEquals(projectList.subList(0, 5), actualProjectList);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.findById(project.getId()));
            Assert.assertEquals(project, projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(projectRepository.findById(id));
    }

    @Test
    public void testFindByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(projectRepository.findByIdForUser(USER_ID_1, project.getId()));
                Assert.assertEquals(project, projectRepository.findById(project.getId()));
            } else Assert.assertNull(projectRepository.findByIdForUser(USER_ID_1, project.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(projectRepository.getSize() - 1);
        @NotNull final Project expected3 = projectList.get(projectRepository.getSize() / 2);
        Assert.assertEquals(expected1, projectRepository.findByIndex(0));
        Assert.assertEquals(expected2, projectRepository.findByIndex(projectRepository.getSize() - 1));
        Assert.assertEquals(expected3, projectRepository.findByIndex(projectRepository.getSize() / 2));
        Assert.assertNull(projectRepository.findByIndex(projectRepository.getSize()));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(5);
        Assert.assertEquals(expected1, projectRepository.findByIndexForUser(USER_ID_1, 0));
        Assert.assertEquals(expected2, projectRepository.findByIndexForUser(USER_ID_2, 0));
        Assert.assertNull(projectRepository.findByIndexForUser("unknown user", 0));
    }

    @Test
    public void testFindByName() {
        for (@NotNull final Project project : projectList) {
            Assert.assertEquals(project, projectRepository.findByName(project.getName()));
        }
        Assert.assertNull(projectRepository.findByName("???"));
    }

    @Test
    public void testFindByNameForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertEquals(project, projectRepository.findByNameForUser(USER_ID_1, project.getName()));
            else Assert.assertNull(projectRepository.findByNameForUser(USER_ID_1, project.getName()));
        }
    }

    @Test
    public void testFindKeysForUser() {
        @NotNull final List<String> expectedList = new ArrayList<>();
        @NotNull final List<String> emptyList = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) expectedList.add(project.getId());
        }
        Assert.assertEquals(expectedList, projectRepository.findKeysForUser(USER_ID_1));
        Assert.assertEquals(emptyList, projectRepository.findKeysForUser("???"));
    }

    @Test
    public void testGetId() {
        int count = 0;
        for (@NotNull final Project project : projectList) {
            Assert.assertEquals(project.getId(), projectRepository.getId(project.getName()));
            Assert.assertEquals(project.getId(), projectRepository.getId(count));
            count++;
        }
        Assert.assertNull(projectRepository.getId("Unknown name"));
        Assert.assertNull(projectRepository.getId(projectRepository.getSize()));
    }

    @Test
    public void testGetIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertEquals(project.getId(), projectRepository.getIdForUser(USER_ID_1, project.getName()));
            else Assert.assertNull(project.getId(), projectRepository.getIdForUser(USER_ID_1, project.getName()));
        }
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(5, projectRepository.getSizeForUser(USER_ID_1));
        Assert.assertEquals(0, projectRepository.getSizeForUser("Unknown user"));
    }

    @Test
    public void testIsEmpty() {
        Assert.assertFalse(projectRepository.isEmpty());
        @NotNull IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.isEmpty());
        projectRepository.clear();
        Assert.assertTrue(projectRepository.isEmpty());
    }

    @Test
    public void testIsEmptyForUser() {
        Assert.assertFalse(projectRepository.isEmptyForUser(USER_ID_1));
        Assert.assertTrue(projectRepository.isEmptyForUser("Unknown user"));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(projectRepository.isNotFoundById(validId));
        Assert.assertTrue(projectRepository.isNotFoundById(invalidId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectList.get(0).getId();
        Assert.assertFalse(projectRepository.isNotFoundByIdForUser(USER_ID_1, validId));
        Assert.assertTrue(projectRepository.isNotFoundByIdForUser(USER_ID_2, validId));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Project project : projectList) {
            projectRepository.remove(project);
            Assert.assertNull(projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Project projectNotFromRepository = new Project();
        projectRepository.remove(projectNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.removeById(project.getId()));
            Assert.assertNull(projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        Assert.assertNull(projectRepository.removeById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(projectRepository.removeByIdForUser(USER_ID_1, project.getId()));
                Assert.assertNull(projectRepository.findByIdForUser(USER_ID_1, project.getId()));
            } else {
                Assert.assertNull(projectRepository.removeByIdForUser(USER_ID_1, project.getId()));
            }
        }
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(projectRepository.getSize() - 1);
        @NotNull final Project expected3 = projectList.get(projectRepository.getSize() / 2);
        Assert.assertEquals(expected1, projectRepository.removeByIndex(0));
        Assert.assertEquals(expected2, projectRepository.removeByIndex(projectRepository.getSize() - 1));
        Assert.assertEquals(expected3, projectRepository.removeByIndex(projectRepository.getSize() / 2));
        Assert.assertNull(projectRepository.removeByIndex(projectRepository.getSize()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 3, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(5);
        Assert.assertEquals(expected1, projectRepository.removeByIndexForUser(USER_ID_1, 0));
        Assert.assertEquals(expected2, projectRepository.removeByIndexForUser(USER_ID_2, 0));
        Assert.assertNull(projectRepository.removeByIndexForUser("unknown user", 0));
    }

    @Test
    public void testRemoveByName() {
        for (@NotNull final Project project : projectList) {
            Assert.assertEquals(project, projectRepository.removeByName(project.getName()));
        }
        Assert.assertNull(projectRepository.removeByName("???"));
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testRemoveByNameForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertEquals(project, projectRepository.removeByNameForUser(USER_ID_1, project.getName()));
            else Assert.assertNull(projectRepository.removeByNameForUser(USER_ID_1, project.getName()));
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES - 5, projectRepository.getSize());
    }

    @Test
    public void testUpdatePositive() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectExpected.setName(newName);
        projectExpected.setDescription(newDescription);
        @Nullable final Project projectActual = projectRepository.update(projectId, newName, newDescription);
        Assert.assertEquals(projectExpected, projectActual);
    }

    @Test
    public void testUpdateNegative() {
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        @Nullable final Project projectActual = projectRepository.update(projectId, newName, newDescription);
        Assert.assertNull(projectActual);
    }

    @Test
    public void testUpdateForUserPositive() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectExpected.setName(newName);
        projectExpected.setDescription(newDescription);
        @Nullable final Project projectActual = projectRepository.updateForUser(USER_ID_1, projectId, newName, newDescription);
        Assert.assertEquals(projectExpected, projectActual);
    }

    @Test
    public void testUpdateForUserNegative() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectExpected.setName(newName);
        projectExpected.setDescription(newDescription);
        @Nullable final Project projectActual = projectRepository.updateForUser(USER_ID_2, projectId, newName, newDescription);
        Assert.assertNull(projectActual);
    }

    @Test
    public void testUpdateStatusById() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        projectExpected.setStatus(Status.COMPLETED);
        @Nullable final Project projectActual = projectRepository.updateStatusById(projectId, Status.COMPLETED);
        Assert.assertNotNull(projectActual);
        Assert.assertEquals(projectExpected, projectActual);
        Assert.assertNotNull(projectActual.getDateFinish());
    }

    @Test
    public void testUpdateStatusByIdForUser() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        projectExpected.setStatus(Status.IN_PROGRESS);
        @Nullable final Project projectActual = projectRepository.updateStatusByIdForUser(USER_ID_1, projectId, Status.IN_PROGRESS);
        Assert.assertNotNull(projectActual);
        Assert.assertEquals(projectExpected, projectActual);
        Assert.assertNotNull(projectActual.getDateStart());
        Assert.assertNull(projectRepository.updateStatusByIdForUser(USER_ID_2, projectId, Status.IN_PROGRESS));
    }

    @Test
    public void testUpdateStatusByIndex() {
        @NotNull final Project projectExpected = projectList.get(0);
        projectExpected.setStatus(Status.COMPLETED);
        @Nullable final Project projectActual = projectRepository.updateStatusByIndex(0, Status.COMPLETED);
        Assert.assertNotNull(projectActual);
        Assert.assertEquals(projectExpected, projectActual);
        Assert.assertNotNull(projectActual.getDateFinish());
    }

    @Test
    public void testUpdateStatusByIndexForUser() {
        @NotNull final Project projectExpected = projectList.get(0);
        projectExpected.setStatus(Status.IN_PROGRESS);
        @Nullable final Project projectActual = projectRepository.updateStatusByIndexForUser(USER_ID_1, 0, Status.IN_PROGRESS);
        Assert.assertNotNull(projectActual);
        Assert.assertEquals(projectExpected, projectActual);
        Assert.assertNotNull(projectActual.getDateStart());
        Assert.assertNull(projectRepository.updateStatusByIndexForUser("unknown user", 0, Status.IN_PROGRESS));
    }

    @Test
    public void testUpdateStatusByName() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectName = projectExpected.getName();
        projectExpected.setStatus(Status.COMPLETED);
        @Nullable final Project projectActual = projectRepository.updateStatusByName(projectName, Status.COMPLETED);
        Assert.assertNotNull(projectActual);
        Assert.assertEquals(projectExpected, projectActual);
        Assert.assertNotNull(projectActual.getDateFinish());
    }

    @Test
    public void testUpdateStatusByNameForUser() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectName = projectExpected.getName();
        projectExpected.setStatus(Status.IN_PROGRESS);
        @Nullable final Project projectActual = projectRepository.updateStatusByNameForUser(USER_ID_1, projectName, Status.IN_PROGRESS);
        Assert.assertNotNull(projectActual);
        Assert.assertEquals(projectExpected, projectActual);
        Assert.assertNotNull(projectActual.getDateStart());
        Assert.assertNull(projectRepository.updateStatusByIdForUser(USER_ID_2, projectName, Status.IN_PROGRESS));
    }

}

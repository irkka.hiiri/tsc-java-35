package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.List;

public interface IBusinessEntityService<E extends AbstractBusinessEntity> extends IService<E> {

    void add(@NotNull String userId, @NotNull String entityName, @Nullable String entityDescription);

    void clear(String userId);

    @Nullable E completeById(@NotNull String userId,
                             @NotNull String projectId);

    @Nullable E completeByIndex(@NotNull String userId, int projectIndex);

    @Nullable E completeByName(@NotNull String userId, @NotNull String projectName);

    @NotNull List<E> findAll(@NotNull String userId);

    @NotNull List<E> findAll(@NotNull String userId, @NotNull String sortBy);

    @Nullable E findById(@NotNull String userId, @Nullable String entityId);

    @Nullable E findByIndex(@NotNull String userId, int entityIndex);

    @Nullable E findByName(@NotNull String userId, @NotNull String entityName);

    @Nullable String getId(@NotNull String userId, int entityIndex);

    @Nullable String getId(@NotNull String userId, @NotNull String entityName);

    int getSize(@NotNull String userId);

    boolean isEmpty(@NotNull String userId);

    boolean isNotFoundById(@NotNull String userId, @NotNull String entityId);

    @Nullable E removeById(@NotNull String userId, @NotNull String projectId);

    @Nullable E removeByIndex(@NotNull String userId, int projectIndex);

    @Nullable E removeByName(@NotNull String userId, @NotNull String entityName);

    @Nullable E startById(@NotNull String userId, @NotNull String projectId);

    @Nullable E startByIndex(@NotNull String userId, int projectIndex);

    @Nullable E startByName(@NotNull String userId, @NotNull String projectName);

    @Nullable E updateById(@NotNull String userId,
                           @NotNull String entityId,
                           @NotNull String entityName,
                           @Nullable String entityDescription);

    @Nullable
    E updateByIndex(@NotNull String userId,
                    int entityIndex,
                    @NotNull String entityName,
                    @Nullable String entityDescription);

    @Nullable
    E updateStatusById(@NotNull String userId,
                       @NotNull String entityId,
                       @NotNull Status status);

    @Nullable
    E updateStatusByIndex(@NotNull String userId,
                          int projectIndex,
                          @NotNull Status status);

    @Nullable
    E updateStatusByName(@NotNull String userId,
                         @NotNull String projectName,
                         @NotNull Status status);
}

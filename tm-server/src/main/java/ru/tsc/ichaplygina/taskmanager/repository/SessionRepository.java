package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.model.Session;

public class SessionRepository extends AbstractModelRepository<Session> implements ISessionRepository {

}

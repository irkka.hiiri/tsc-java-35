package ru.tsc.ichaplygina.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.model.Session;

public interface ISessionRepository extends IRepository<Session> {
    void add(@NotNull Session session);
}

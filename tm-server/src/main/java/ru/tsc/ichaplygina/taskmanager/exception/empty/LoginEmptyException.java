package ru.tsc.ichaplygina.taskmanager.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class LoginEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Login is empty.";

    public LoginEmptyException() {
        super(MESSAGE);
    }

}

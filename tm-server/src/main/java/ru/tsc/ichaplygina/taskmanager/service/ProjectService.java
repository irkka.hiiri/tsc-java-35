package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public final class ProjectService extends AbstractBusinessEntityService<Project> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository, @NotNull final IUserService userService) {
        super(repository, userService);
    }

}
